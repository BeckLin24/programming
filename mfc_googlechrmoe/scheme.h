#ifndef _CEFCLIENT_SCHEME_TEST
#define _CEFCLIENT_SCHEME_TEST

#include "include/cef.h"

// Register the scheme handler.
void InitSchemeHandler();

#endif // _CEFCLIENT_SCHEME_TEST
